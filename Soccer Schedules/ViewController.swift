//
//  ViewController.swift
//  Soccer Schedules
//
//  Created by Naveen Dushila on 2019-03-05.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        matchData.teamASelected = [matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement()] as! [String]
//
//        matchData.teamBSelected = [matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement(),matchData.teamName.randomElement()] as! [String]
//
//        matchData.dateSelected = [matchData.dates.randomElement(), matchData.dates.randomElement(), matchData.dates.randomElement(), matchData.dates.randomElement(), matchData.dates.randomElement(), matchData.dates.randomElement(), matchData.dates.randomElement(), matchData.dates.randomElement(), matchData.dates.randomElement(), matchData.dates.randomElement()] as! [Int]
//
//        matchData.dateSelected.sort()
        
        
        if(WCSession.isSupported())
        {
            print("CONNECTED")
            let session =  WCSession.default
            session.delegate = self
            session.activate()
        }
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fixtures", for: indexPath) as! ViewControllerTableViewCell
        
        cell.teamA.image = UIImage(named: matchData.teamASelected[indexPath.row])
        cell.teamB.image = UIImage(named: matchData.teamBSelected[indexPath.row])

        cell.dateLabel.text = matchData.dateSelected[indexPath.row] + "/05/2019"
        
        cell.timeLabel.text = matchData.timeHours.randomElement()!  + " : 00"
        
        cell.subBtnOut.tag = indexPath.row
        
        cell.gameLabel.text = String(indexPath.row)
        
        return cell
    }
    
    

}

