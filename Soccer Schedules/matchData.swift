//
//  matchData.swift
//  Soccer Schedules
//
//  Created by Naveen Dushila on 2019-03-07.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import Foundation
import UIKit

class matchData:UIViewController {

   static var teams: [UIImage] = [UIImage(named: "Afghanistan")!, UIImage(named: "Aland")!, UIImage(named:"Albania")!, UIImage(named:"Algeria")!, UIImage(named:"American Samoa")!, UIImage(named:"Andorra")!, UIImage(named:"Angola")!, UIImage(named:"Anguilla")!, UIImage(named:"Antarctica")!, UIImage(named:"Antigua and Barbuda")!, UIImage(named:"Argentina")!, UIImage(named:"Armenia")!, UIImage(named:"Aruba")!, UIImage(named:"Australia")!, UIImage(named:"Austria")!, UIImage(named:"Azerbaijan")!, UIImage(named:"Bahamas")!, UIImage(named:"Bahrain")!, UIImage(named:"Bangladesh")!, UIImage(named:"Barbados")!, UIImage(named:"Belarus")!, UIImage(named:"Belgium")!, UIImage(named:"Belize")!, UIImage(named:"Benin")!, UIImage(named:"Bermuda")!, UIImage(named:"Bhutan")!, UIImage(named:"Bolivia")!, UIImage(named:"Bonaire")!]
    
    static var teamName: [String] = ["Afghanistan","Aland", "Albania", "Algeria" , "American Samoa" , "Andorra" , "Angola" , "Anguilla" , "Antarctica" , "Antigua and Barbuda" , "Argentina" , "Armenia" , "Aruba" , "Australia" , "Austria" , "Azerbaijan" , "Bahamas" , "Bahrain" , "Bangladesh" , "Barbados" , "Belarus" , "Belgium" , "Belize" , "Benin" , "Bermuda" , "Bhutan"  , "Bolivia" , "Bonaire"]
    
    static var dates: [Int] = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
    
    static var timeHours: [String] = ["09", "10", "11", "12", "13", "14", "15", "16"]
    
    static var teamASelected: [String] = ["Bonaire", "Belarus", "Aruba" , "Australia" , "Austria" , "Azerbaijan" , "Bahamas" , "Bahrain" , "Bangladesh" , "Barbados" ]
    static var teamBSelected: [String] = ["Afghanistan","Aland", "Albania", "Algeria" , "American Samoa" , "Andorra" , "Angola" , "Anguilla" , "Antarctica" , "Antigua and Barbuda"]
    static var dateSelected: [String] = ["3","7", "9" , "11" , "13", "15" , "16", "17", "17", "18"]
    
    static var matchSubs: Int = 0
    
    static var itemsRemoved: Int = 0
    
    static var oldTag: [Int] = []
    
    static var matchCodes:[Int] = [0,1,2,3,4,5,6,7,8,9]
    
}
