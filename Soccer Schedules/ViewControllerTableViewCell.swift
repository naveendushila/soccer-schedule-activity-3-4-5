//
//  ViewControllerTableViewCell.swift
//  Soccer Schedules
//
//  Created by Naveen Dushila on 2019-03-07.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewControllerTableViewCell: UITableViewCell,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    @IBOutlet weak var gameLabel: UILabel!
    
    @IBOutlet weak var teamA: UIImageView!
    
    @IBOutlet weak var teamB: UIImageView!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var subBtnOut: UIButton!
    
    @IBAction func subsBtn(_ sender: UIButton) {
        
        
      if(WCSession.default.isReachable)
       {
            if (sender.title(for: .normal) == "Subscribe")
            {
                print("Subs Btn Pressed")
                
                let btnTag = sender.tag
                
                matchData.oldTag.append(sender.tag)
                
                sender.tag = matchData.matchSubs
                
                matchData.matchSubs = matchData.matchSubs + 1
                
                let teams = ["TeamA": matchData.teamASelected[btnTag], "TeamB": matchData.teamBSelected[btnTag], "MatchDate": matchData.dateSelected[btnTag], "MatchCode": String (sender.tag)]
            
                print("Match Subs: \(sender.tag)")
                
                WCSession.default.sendMessage(teams, replyHandler: nil)
                sender.setTitle("Unsubscribe", for: .normal)
            }
            else
            {
                print("game is already subscribed!")
               
                let tempTag = sender.tag
                
                print ("Before Process")
                
                print("Match Subs: \(matchData.matchSubs)")
                
                print("Items Removed: \(matchData.itemsRemoved)")
                
                print("Sender Tag: \(sender.tag)")
                
                matchData.matchSubs = matchData.matchSubs - 1
                
//                if (sender.tag < matchData.matchSubs) {
//                    if (matchData.matchSubs == 0 || sender.tag == 0) {
//                        sender.tag = 0
//                    }
//                    else {
//                        sender.tag = sender.tag - matchData.itemsRemoved
//                        
//                    }
//                }
//                
                
                let matchUnsub = ["TeamA" : "", "TeamB" : "", "MatchDate" : "", "MatchCode" : String (sender.tag)]
                
                
                print("Match Un-Subs: \(sender.tag)")
                
               
                
                matchData.itemsRemoved = matchData.itemsRemoved + 1
                
                WCSession.default.sendMessage(matchUnsub, replyHandler: nil)
               
                sender.tag = matchData.oldTag[tempTag]
                sender.setTitle("Subscribe", for: .normal)
                
                print ("After Process")
                
                print("Match Subs: \(matchData.matchSubs)")
                
                print("Items Removed: \(matchData.itemsRemoved)")
                
                print("Sender Tag: \(sender.tag)")
                
            }
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
