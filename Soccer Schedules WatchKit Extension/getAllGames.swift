//
//  getAllGames.swift
//  Soccer Schedules WatchKit Extension
//
//  Created by Naveen Dushila on 2019-03-08.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import WatchKit
import Foundation


class getAllGames: WKInterfaceController {

    
    @IBOutlet weak var allgamesTable: WKInterfaceTable!
    
    

    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
            super.willActivate()
         self.allgamesTable.setNumberOfRows(subsGameData.teamASelected.count, withRowType: "allGames")
        
        for(i, teamA) in subsGameData.teamASelected.enumerated() {
            let row = allgamesTable.rowController(at: i) as! allGamesController
            row.teamA.setImage(UIImage(named: subsGameData.teamASelected[i]))
            row.teamB.setImage(UIImage(named: subsGameData.teamBSelected[i]))
            row.dateTime.setText(subsGameData.dateSelected[i] + "/05/19")
            
        
    }
    
    
    }
}
