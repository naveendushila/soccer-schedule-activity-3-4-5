//
//  InterfaceController.swift
//  Soccer Schedules WatchKit Extension
//
//  Created by Naveen Dushila on 2019-03-05.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet weak var msgLabel: WKInterfaceLabel!
    
    @IBOutlet weak var subGamesTable: WKInterfaceTable!
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
       

        updateSubsList()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        WKInterfaceDevice().play(.click)
        print("GOT THE MESSAGE \(message)")
        let teamA = message["TeamA"] as! String
        let teamB = message["TeamB"] as! String
        let matchDate = message["MatchDate"] as! String
        let matchCode = message["MatchCode"] as! String
    
        
        
        
        if  (teamA == "" && teamB == "" && matchDate == "") {
            
            let isIndexValid = subsGameData.teamA.contains(matchCode)
            if (isIndexValid == true) {
                
                for i in 0...subsGameData.matchCodes.count {
                    if (subsGameData.matchCodes[i] == Int(matchCode)!) {
                        print("Found the game to remove: matchCode = \(i)")
                        subsGameData.teamA.remove(at: i)
                        subsGameData.teamB.remove(at: i)
                        subsGameData.dates.remove(at: i)
                        subsGameData.matchCodes.remove(at: i)
                        break;
                        
                    }
                }
//
//
//                print("Match Code to remove: \(matchCode)")
//                subsGameData.teamA.remove(at: Int(matchCode)!)
//                subsGameData.teamB.remove(at: Int(matchCode)!)
//                subsGameData.dates.remove(at: Int(matchCode)!)
            }
            else {
                print("Game with matchCode \(matchCode) was already removed")
            }
            
        }
        else {
            
            print("TeamA: \(teamA)")
            print("TeamB: \(teamB)")
            print("Date: \(matchDate)")
            print("Match Code: \(matchCode)")
            
            subsGameData.teamA.append(teamA)
            subsGameData.teamB.append(teamB)
            subsGameData.dates.append(matchDate)
            subsGameData.matchCodes.append(Int(matchCode)!)
        }
        
        updateSubsList()
    }
    
    func updateSubsList() {
        if (subsGameData.teamA.count == 0) {
            msgLabel.setText("No Subscribed Game!")
            
        }
        else {
            //self.willActivate()
            msgLabel.setText("")
            msgLabel.setHidden(true)
            self.subGamesTable.setNumberOfRows(subsGameData.teamA.count, withRowType: "subGames")
            
            for(i, teamA) in subsGameData.teamA.enumerated() {
                let row = subGamesTable.rowController(at: i) as! subGamesController
                row.teamAImg.setImage(UIImage(named: subsGameData.teamA[i]))
                row.teamBImg.setImage(UIImage(named: subsGameData.teamB[i]))
                row.dateLabel.setText(subsGameData.dates[i] + "/05/19")
                
            }
            
        }
        
        
    }

}
