//
//  allGamesController.swift
//  Soccer Schedules WatchKit Extension
//
//  Created by Naveen Dushila on 2019-03-08.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import WatchKit

class allGamesController: NSObject {

    @IBOutlet weak var teamA: WKInterfaceImage!
    
    @IBOutlet weak var teamB: WKInterfaceImage!
    
    @IBOutlet weak var dateTime: WKInterfaceLabel!
    
}
