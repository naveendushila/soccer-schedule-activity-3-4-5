//
//  appLaucher.swift
//  Soccer Schedules WatchKit Extension
//
//  Created by Naveen Dushila on 2019-03-08.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class appLaucher: WKInterfaceController, WCSessionDelegate  {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        
    }
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if(WCSession.isSupported())
        {
            print("Phone Connected from App Launcher!")
            let session =  WCSession.default
            session.delegate = self
            session.activate()
        }
   
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        WKInterfaceDevice().play(.click)
        print("GOT THE MESSAGE \(message)")
        let teamA = message["TeamA"] as! String
        let teamB = message["TeamB"] as! String
        let matchDate = message["MatchDate"] as! String
        let matchCode = message["MatchCode"] as! String
        
        
        if  (teamA == "" && teamB == "" && matchDate == "") {
            for i in 0...subsGameData.matchCodes.count {
                if (subsGameData.matchCodes[i] == Int(matchCode)!) {
                    print("Found the game to remove: matchCode = \(i)")
                    subsGameData.teamA.remove(at: i)
                    subsGameData.teamB.remove(at: i)
                    subsGameData.dates.remove(at: i)
                    subsGameData.matchCodes.remove(at: i)
                    break;
                    
                }
            }
            //
        }
        else {
            
            print("TeamA: \(teamA)")
            print("TeamB: \(teamB)")
            print("Date: \(matchDate)")
            print("Match Code: \(matchCode)")
            
            subsGameData.teamA.append(teamA)
            subsGameData.teamB.append(teamB)
            subsGameData.dates.append(matchDate)
            subsGameData.matchCodes.append(Int(matchCode)!)
        }
        
    }
    

    
    
}
