//
//  subsGameData.swift
//  Soccer Schedules WatchKit Extension
//
//  Created by Naveen Dushila on 2019-03-08.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import WatchKit

class subsGameData: NSObject {

    static var teamA: [String] = []
    static var teamB: [String] = []
    static var dates: [String] = []
    static var matchCodes:[Int] = []
    
    static var timeHours: [String] = ["09", "10", "11", "12", "13", "14", "15", "16"]
    
    static var teamASelected: [String] = ["Bonaire", "Belarus", "Aruba" , "Australia" , "Austria" , "Azerbaijan" , "Bahamas" , "Bahrain" , "Bangladesh" , "Barbados" ]
    static var teamBSelected: [String] = ["Afghanistan","Aland", "Albania", "Algeria" , "American Samoa" , "Andorra" , "Angola" , "Anguilla" , "Antarctica" , "Antigua and Barbuda"]
    static var dateSelected: [String] = ["3","7", "9" , "11" , "13", "15" , "16", "17", "17", "18"]
    
}
